package gmihtml

import (
	"html/template"
	"testing"
)

func TestTextElement(t *testing.T) {
	var el Element = TextElement{raw: "hello world"}
	if el.Raw() != "hello world" {
		t.Errorf("TextElement.Raw() was incorrect, got: %s, want: %s.", el.Raw(), "hello world")
	}

	if el.Type() != Text {
		t.Errorf("TextElement.Type() was incorrent, got: %s, want: %s.", el.Type(), Text)
	}

	template, _ := template.New(string(Text)).Parse("<span>{{.Raw}}</span>")

	result, err := ToHTML(template, el)
	expect := "<span>hello world</span>"
	if err != nil {
		t.Errorf("Got error: %s while running TextElement.HTML test", err.Error())
	}
	if string(result) != expect {
		t.Errorf("TextElement.HTML was incorrect, got: %s, want: %s.", result, expect)
	}
}

func TestLinkElement(t *testing.T) {
	tables := []struct {
		raw   string
		link  string
		label string
		html  template.HTML
	}{
		{
			"=> /valid link",
			"/valid",
			"link",
			`<a href="/valid">link</a>`,
		},
		{
			"=>gemini://gemini.circumlunar.space",
			"gemini://gemini.circumlunar.space",
			"",
			`<a href="gemini://gemini.circumlunar.space">gemini://gemini.circumlunar.space</a>`,
		},
	}

	template, _ := template.New(string(Link)).Parse(`<a href="{{.Target}}">{{.Label}}</a>`)

	for _, table := range tables {
		el := newLinkElement(table.raw)
		if el.Raw() != table.raw {
			t.Errorf("LinkElement.Raw() was incorrect, got: %s, want: %s.", el.Raw(), table.raw)
		}

		if el.Type() != Link {
			t.Errorf("LinkElement.Type() was incorrent, got: %s, want: %s.", el.Type(), Link)
		}

		result, err := ToHTML(template, el)

		if err != nil {
			t.Errorf("Got error: %s while running LinkElement.HTML test on %s", err.Error(), table.raw)
		}
		if result != table.html {
			t.Errorf("LinkElement.HTML was incorrect, got: %s, want: %s.", result, table.html)
		}
	}

}

func TestHeadingElement(t *testing.T) {
	tables := []struct {
		raw   string
		depth int
		text  string
		html  template.HTML
	}{
		{
			"### Some heading  ",
			3,
			"Some heading",
			`<h3>Some heading</h3>`,
		},
		{
			"#Title",
			1,
			"Title",
			`<h1>Title</h1>`,
		},
		{
			"########### over the top",
			11,
			"over the top",
			`<h5>over the top</h5>`,
		},
	}

	template, _ := template.New(string(Heading)).Parse(`{{index .Hx 0}}>{{.Text}}{{index .Hx 1}}`)

	for _, table := range tables {
		el := newHeadingElement(table.raw)
		if el.Raw() != table.raw {
			t.Errorf("HeadingElement.Raw() was incorrect, got: %s, want: %s.", el.Raw(), table.raw)
		}

		if el.Type() != Heading {
			t.Errorf("HeadingElement.Type() was incorrent, got: %s, want: %s.", el.Type(), Heading)
		}

		result, err := ToHTML(template, el)

		if err != nil {
			t.Errorf("Got error: %s while running ToHTML on HeadingElement test on %s", err.Error(), table.raw)
		}
		if result != table.html {
			t.Errorf("HeadingElement HTML conversion was incorrect, got: %s, want: %s.", result, table.html)
		}
	}

}

func TestQuoteElement(t *testing.T) {
	tables := []struct {
		raw, text string
		html      template.HTML
	}{
		{"> some quote", "some quote", "<blockquote>some quote</blockquote>"},
		{">another quote", "another quote", "<blockquote>another quote</blockquote>"},
	}

	for _, quote := range tables {
		var el QuoteElement = newQuoteElement(quote.raw).(QuoteElement)
		if el.Raw() != quote.raw {
			t.Errorf("QuoteElement.Raw() was incorrect, got: %s, want: %s.", el.Raw(), quote.raw)
		}

		if el.Text() != quote.text {
			t.Errorf("QuoteElement.Text() was incorrect, got: %s, want: %s.", el.Text(), quote.text)
		}

		if el.Type() != Quote {
			t.Errorf("QuoteElement.Type() was incorrent, got: %s, want: %s.", el.Type(), Quote)
		}

		template, _ := template.New(string(Quote)).Parse("<blockquote>{{.Text}}</blockquote>")
		result, err := ToHTML(template, el)

		if err != nil {
			t.Errorf("Got error: %s while running ToHTML on QuoteElement test", err.Error())
		}
		if result != quote.html {
			t.Errorf("QuoteElement HTML conversion was incorrect, got: %s, want: %s.", result, quote.html)
		}
	}
}

func TestListItemElement(t *testing.T) {
	tables := []struct {
		raw, text string
		html      template.HTML
	}{
		{"* some item", "some item", "<li>some item</li>"},
		{"*another item", "another item", "<li>another item</li>"},
	}

	for _, item := range tables {
		var el ListItemElement = newListItemElement(item.raw).(ListItemElement)
		if el.Raw() != item.raw {
			t.Errorf("ListItemElement.Raw() was incorrect, got: %s, want: %s.", el.Raw(), item.raw)
		}

		if el.Text() != item.text {
			t.Errorf("ListItemElement.Text() was incorrect, got: %s, want: %s.", el.Text(), item.text)
		}

		if el.Type() != ListItem {
			t.Errorf("ListItemElement.Type() was incorrent, got: %s, want: %s.", el.Type(), ListItem)
		}

		template, _ := template.New(string(ListItem)).Parse("<li>{{.Text}}</li>")
		result, err := ToHTML(template, el)

		if err != nil {
			t.Errorf("Got error: %s while running ToHTML on ListItemElement test", err.Error())
		}
		if result != item.html {
			t.Errorf("ListItemElement HTML conversion was incorrect, got: %s, want: %s.", result, item.html)
		}
	}
}

func TestPreformattedElement(t *testing.T) {
	tables := []struct {
		raw  string
		body template.HTML
		alt  template.HTMLAttr
		html template.HTML
	}{
		{"```alt text", ">>> the body <<<", "alt text", `<pre alt="alt text">>>> the body <<<</pre>`},
		{"```          spaced out alt text", ` the body
with new lines`, "spaced out alt text", `<pre alt="spaced out alt text"> the body
with new lines</pre>`},
	}

	for _, pre := range tables {
		var el PreformattedElement = newPreformattedElement(pre.raw).(PreformattedElement)
		el.Body = pre.body
		if el.Raw() != pre.raw {
			t.Errorf("PreformattedElement.Raw() was incorrect, got: %s, want: %s.", el.Raw(), pre.raw)
		}

		if el.Alt() != pre.alt {
			t.Errorf("PreformattedElement.Alt() was incorrect, got: %s, want: %s.", el.Alt(), pre.alt)
		}

		if el.Type() != Preformatted {
			t.Errorf("PreformattedElement.Type() was incorrent, got: %s, want: %s.", el.Type(), Preformatted)
		}

		template, _ := template.New(string(Preformatted)).Parse(`<pre alt="{{.Alt}}">{{.Body}}</pre>`)
		result, err := ToHTML(template, el)

		if err != nil {
			t.Errorf("Got error: %s while running ToHTML on PreformattedElement test", err.Error())
		}
		if result != pre.html {
			t.Errorf("PreformattedElement HTML conversion was incorrect, got: %s, want: %s.", result, pre.html)
		}
	}
}

func TestMissingTemplate(t *testing.T) {
	el := newLinkElement("=> some link")

	template, _ := template.New("").Parse(`<a>{{.Target}}</a>`)
	result, err := ToHTML(template, el)

	if err == nil {
		t.Errorf("Expected non-nil error, got %s", result)
	}
}

func TestBadTemplate(t *testing.T) {
	el := newLinkElement("=> some link")

	// .Bad should be .Target
	template, _ := template.New(string(Link)).Parse(`<a href="{{.Bad}}">{{.Label}}</a>`)
	result, err := ToHTML(template, el)

	if err == nil {
		t.Errorf("Expected non-nil error, got %s", result)
	}
}

func TestNewElement(t *testing.T) {
	tables := []struct {
		raw          string
		expectedType ElementType
	}{
		{"=> /some link", Link},
		{"* some listItem", ListItem},
		{"> some quote", Quote},
		{"```some pre", Preformatted},
		{"## some heading", Heading},
		{"# another heading", Heading},
		{"just text", Text},
		{"", Text},
		{" => bad link", Text},
	}

	for _, table := range tables {
		el := NewElement(table.raw)
		if el.Type() != table.expectedType {
			t.Errorf("NewElement created incorrect type from %s, got: %s, expected: %s.", table.raw, el.Type(), table.expectedType)
		}
	}
}
