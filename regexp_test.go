package gmihtml

import (
	"testing"
)

func TestLinkRegexp(t *testing.T) {
	goodlinks := []struct {
		raw    string
		target string
		label  string
	}{
		{"=> http://google.com Google", "http://google.com", "Google"},
		{"=> /somelink    Soome ;alskjdf  ", "/somelink", "Soome ;alskjdf  "},
		{"=>kajshdf", "kajshdf", ""},
	}

	for _, link := range goodlinks {
		matches := linkRegexp.FindStringSubmatch(link.raw)
		if matches[0] != link.raw {
			t.Errorf("Full match was incorrect, got: %s, want: %s.", matches[0], link.raw)
		}
		if matches[1] != link.target {
			t.Errorf("Target match was incorrent, got: %s, want: %s.", matches[1], link.target)
		}
		if matches[2] != link.label {
			t.Errorf("Label match was incorrent, got: %s, want: %s.", matches[2], link.label)
		}
	}

	badlinks := []struct {
		raw         string
		shouldMatch bool
	}{
		{"         => http://google.com Google", false},
		{"= > /somelink    Some Link  ", false},
		{"=>", false},
		{"> a quote", false},
	}

	for _, link := range badlinks {
		match := linkRegexp.MatchString(link.raw)
		if match != link.shouldMatch {
			t.Errorf("Regexp match was incorrect, got: %v, want: %v.", match, link.shouldMatch)
		}
	}
}

func TestQuoteRegexp(t *testing.T) {
	goodquotes := []struct {
		raw  string
		text string
	}{
		{"> some quote", "some quote"},
		{">                   some quote", "some quote"},
		{">some quote             ", "some quote             "},
	}

	for _, quote := range goodquotes {
		matches := quoteRegexp.FindStringSubmatch(quote.raw)
		if matches[0] != quote.raw {
			t.Errorf("Full match was incorrect, got: %s, want: %s.", matches[0], quote.raw)
		}
		if matches[1] != quote.text {
			t.Errorf("Text match was incorrent, got: %s, want: %s.", matches[1], quote.text)
		}
	}

	badquotes := []struct {
		raw         string
		shouldMatch bool
	}{
		{"  > leading spaces", false},
		{"< wrong direction", false},
		{"=> /not a quote", false},
		{"also not a quote", false},
	}

	for _, quote := range badquotes {
		match := quoteRegexp.MatchString(quote.raw)
		if match != quote.shouldMatch {
			t.Errorf("Regexp match was incorrect, got: %v, want: %v.", match, quote.shouldMatch)
		}
	}
}

func TestListItemRegexp(t *testing.T) {
	gooditems := []struct {
		raw  string
		text string
	}{
		{"* some item", "some item"},
		{"*                   some item", "some item"},
		{"*some item             ", "some item             "},
	}

	for _, item := range gooditems {
		matches := listItemRegexp.FindStringSubmatch(item.raw)
		if matches[0] != item.raw {
			t.Errorf("Full match was incorrect, got: %s, want: %s.", matches[0], item.raw)
		}
		if matches[1] != item.text {
			t.Errorf("Text match was incorrent, got: %s, want: %s.", matches[1], item.text)
		}
	}

	baditems := []struct {
		raw         string
		shouldMatch bool
	}{
		{"  * leading spaces", false},
		{"+ wrong character", false},
		{"=> /not an item", false},
		{"also not an item", false},
	}

	for _, item := range baditems {
		match := listItemRegexp.MatchString(item.raw)
		if match != item.shouldMatch {
			t.Errorf("Regexp match was incorrect, got: %v, want: %v.", match, item.shouldMatch)
		}
	}
}
