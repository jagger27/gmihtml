package gmihtml

import (
	"html/template"
	"os"
	"testing"
)

func TestScanner(t *testing.T) {
	files := []string{
		"tests/pre.gmi",
		"tests/pre-noclose.gmi",
		"tests/home.gmi",
	}

	for _, name := range files {
		file, _ := os.Open(name)
		tmpl := template.Must(template.ParseFiles("scanner.tmpl", "index.tmpl"))
		container := NewFromReader(file, tmpl)
		ht := container.Generate()
		t.Log(ht)
		container.Tree.Print()
	}

	for _, name := range files {
		file, _ := os.Open(name)
		NewFromReader(file, nil)
	}

}
